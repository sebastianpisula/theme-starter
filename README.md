#LIVE Reload#

Jeżeli chcemy korzystać z `livereload` w wp-config.php należy dodać linijkę:

`define("WP_LIVERELOAD", true);`

#VENDOR#

Wszystkie pliki źródłowe powinny znaleźć się w katalogu `assets/vendor`.

#SPRITES#

Do sprajtów wykorzystywana jest następująca biblioteka: https://github.com/twolfson/gulp.spritesmith. Obrazki dla sprite powinny być zapisywane w katalogu `assets/src/sprites`. Powinny to być obrazki *.png (inne będą ignorowane). W pliku `assets/src/scss/_sprite.scss` tworzone są mixiny, które należy używać. Jeżeli przykładowo plik się nazywa `test.png` w wybranym miejscu należy wykorzystać mixin w taki sposób: `@include sprite($test)`

Nazwa zmiennej jest tworzona z nazwy plików. Warto zaglądać do pliku `assets/src/scss/_sprite.scss` w celu podpatrzenia nazwy zmiennej

#SCSS#
(w budowie)

#JS#
(w budowie)