var gulp = require('gulp');

var runSequence = require('run-sequence');

/**
 * Automatically load any gulp plugins in your package.json
 *
 * @see https://www.npmjs.com/package/gulp-load-plugins
 */
var plugins = require('gulp-load-plugins')();

var spritesmith = require('gulp.spritesmith');
var cssVersioner = require('gulp-css-url-versioner');

var isProduction = !!plugins.util.env.production;
var livereloadStatus = !!plugins.util.env.livereload;

/**
 * 1. Join all scripts from src/js
 * 2. Minify JS
 * 3. Save as app.js in assets/js
 */
gulp.task('js', function () {
    return gulp.src([
        'node_modules/clientjs/dist/client.min.js',
        './assets/src/js/**'
    ])
        .pipe(plugins.concat('app.js'))
        .pipe(isProduction ? plugins.uglify() : plugins.util.noop())
        .pipe(livereloadStatus ? plugins.livereload() : plugins.util.noop())
        .pipe(gulp.dest('./assets/dist/js/'));
});

/**
 * 1. Compile all sass files in src/scss/
 * 2. Minify styles
 * 3. Join all styles
 * 4. Save as style.css and  in assets/styles/css
 * */
gulp.task('sass', function () {
    gulp.src(['./assets/src/scss/**/*.scss'])
        .pipe(!isProduction ? plugins.sourcemaps.init() : plugins.util.noop())
        .pipe(plugins.sass(
            {
                outputStyle: 'compressed',
                includePaths: [
                    require("node-bourbon").includePaths,
                    require("node-neat").includePaths[1],
                    'node_modules/bootstrap-sass/assets/stylesheets/',
                    'node_modules/family.scss/source/src/'
                ]
            }
        ).on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(cssVersioner({version: Math.random(), variable: 'ver'}))
        .pipe(!isProduction ? plugins.sourcemaps.write('.') : plugins.util.noop())
        .pipe(livereloadStatus ? plugins.livereload() : plugins.util.noop())
        .pipe(gulp.dest('./assets/dist/css'));
});

gulp.task('default', ['build'], function () {
    if (livereloadStatus) {
        plugins.livereload.listen(35729);

        gulp.watch('**/*.php').on('change', function (file) {
            plugins.livereload.changed(file.path);
        });
    }

    gulp.watch('assets/src/sprites/**/*.png', {cwd: './'}, ['sprite', 'sass']);
    gulp.watch('assets/src/scss/**', {cwd: './'}, ['sass']);
    gulp.watch('assets/src/js/**', {cwd: './'}, ['js']);
});

gulp.task('sprite', function () {
    var date = new Date();

    var new_date = [
        date.getFullYear().toString(),
        (date.getMonth() + 1).toString(),
        date.getDate().toString(),
        date.getHours().toString(),
        date.getMinutes().toString()
    ];

    var rand = new_date.join('').toString();

    var spriteData = gulp.src('./assets/src/sprites/**/*.png')
        .pipe(spritesmith({
            imgPath: '../img/sprites-' + rand + '.png',
            imgName: 'sprites-' + rand + '.png',
            cssName: '_sprite.scss',
            padding: 20
        }));
    spriteData.img.pipe(gulp.dest('./assets/dist/img/'));
    spriteData.css.pipe(gulp.dest('./assets/src/scss/'));
});

gulp.task('pot', function () {
    return gulp.src('./**/*.php')
        .pipe(plugins.wpPot({
            'domain': 'theme'
            // package: 'Project Name'
        }))
        .pipe(gulp.dest('./languages/theme.pot'));
});

gulp.task('prettify', function () {
    gulp.src('./assets/src/js/**/*')
        .pipe(plugins.jsPrettify({
            "indent_size": 4,
            "indent_char": " ",
            "eol": "\n",
            "indent_level": 0,
            "indent_with_tabs": false,
            "preserve_newlines": true,
            "max_preserve_newlines": 10,
            "jslint_happy": true,
            "space_after_anon_function": true,
            "brace_style": "collapse",
            "keep_array_indentation": false,
            "keep_function_indentation": false,
            "space_before_conditional": true,
            "break_chained_methods": false,
            "eval_code": false,
            "unescape_strings": false,
            "wrap_line_length": 0,
            "wrap_attributes": "auto",
            "wrap_attributes_indent_size": 4,
            "end_with_newline": false
        }))
        .pipe(gulp.dest('./assets/src/js'));
});

gulp.task('clean-dist', function () {
    return gulp.src(['assets/dist/*'], {read: false}).pipe(plugins.clean());
});

gulp.task('build', function (done) {
    runSequence('clean-dist', 'sprite', 'js', 'sass', function () {
        done();
    });
});

gulp.task('tiny-sprite', function () {
    if (!isProduction) {
        return true;
    }

    return gulp.src('./assets/dist/img/**/*')
        .pipe(plugins.tinypng('KEY'))
        .pipe(gulp.dest('./assets/dist/img/'));
});