<?php

namespace Theme;

include get_parent_theme_file_path( 'inc/helpers.php' );
include get_parent_theme_file_path( 'vendor/autoload.php' );

class Theme extends Base {
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Register custom menu
	 */
	public function register_menus() {
		register_nav_menus(
			array(
				'primary' => 'Primary Menu',
			)
		);
	}

	/**
	 * Before main script and style
	 *
	 * @param array $urls
	 * @param array $dirs
	 */
	public function register_scripts_before( $urls, $dirs ) {
		$url = '//maps.googleapis.com/maps/api/js';
		$url = add_query_arg( 'v', 3, $url );
		$url = add_query_arg( 'callback', 'ThemeInitMap', $url );
		$url = add_query_arg( 'key', apply_filters( 'theme/settings/google_api_key', '' ), $url );

		wp_register_script( 'google-maps', $url, array(), '3', true );
	}

	/**
	 * Before main script and style
	 *
	 * @param array $urls
	 * @param array $dirs
	 */
	public function register_scripts_after( $urls, $dirs ) {
		//wp_enqueue_script( 'google-maps' );
	}

	/**
	 * Deps for css
	 *
	 * @param array $deps
	 *
	 * @return array
	 */
	public function style_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for JS
	 *
	 * @param array $deps
	 *
	 * @return array
	 */
	public function script_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for CS and JS
	 *
	 * @param array $deps
	 *
	 * @return array
	 */
	public function both_deps( $deps ) {
		return $deps;
	}

	/**
	 * Main script localize
	 *
	 * @param array $vars
	 *
	 * @return array
	 */
	public function localize_script( $vars ) {
		return $vars;
	}

	/**
	 * Register image sizes
	 */
	public function register_sizes() {
		//set_post_thumbnail_size();
	}

	/**
	 * Register sidebar
	 */
	public function register_sidebars() {
//		register_sidebar(
//			array(
//				'name'          => 'Bottom sidebar',
//				'id'            => 'bottom',
//				'description'   => '',
//				'before_widget' => '<div id="%1$s" class="widget %2$s">',
//				'after_widget'  => '</div>',
//				'before_title'  => '<h2 class="widget-title">',
//				'after_title'   => '</h2>',
//			)
//		);
	}

	/**
	 * Register Widgets
	 */
	public function register_widgets() {
		//register_widget( 'widget class' );
	}
}

new Theme;