<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="header js--header">
    <div class="container">

        <div class="row">
			<?php if ( has_custom_logo() ) : ?>
                <div class="col-md-2">
                    <h1 class="logo-container">
						<?php the_custom_logo(); ?>
                    </h1>
                </div>
			<?php endif; ?>

            <div class="col-md-10">

				<?php if ( is_active_sidebar( 'top' ) ) : ?>
                    <div class="row">
						<?php dynamic_sidebar( 'top' ); ?>
                    </div>
				<?php endif; ?>

				<?php if ( has_nav_menu( 'primary' ) ): ?>
                    <div class="row">
                        <div class="col-md-12">
							<?php
							wp_nav_menu(
								array(
									'theme_location' => 'primary',
									'menu_class'     => 'nav navbar-nav main-menu js--main-menu',
									'container'      => 'nav',
								)
							);
							?>
                        </div>
                    </div>
				<?php endif; ?>
            </div>
        </div>
    </div>
</header>
