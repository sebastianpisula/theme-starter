(function ($) {
    $(document).ready(function () {
        var body = $('body');
        var client = new ClientJS();
        body.attr('browser', client.getBrowser().toLowerCase());
        body.attr('browser-version', client.getBrowserVersion());
        body.attr('browser-version-major', client.getBrowserMajorVersion());
        body.attr('mobile', client.isMobile().toString());
        body.attr('mobile-ios', client.isMobileIOS().toString());
    });
})(jQuery);