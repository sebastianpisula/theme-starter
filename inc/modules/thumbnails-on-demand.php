<?php

namespace Theme\Module;

/**
 * Class Thumbnail_Demand
 * @package Theme\Module
 * @source: http://wordpress.stackexchange.com/a/124790
 */
class Thumbnail_Demand {
	public function __construct() {
		add_filter( 'image_downsize', [ $this, 'image_downsize' ], 5, 3 );
		add_filter( 'intermediate_image_sizes_advanced', [ $this, 'intermediate_image_sizes_advanced' ], 5, 2 );
	}

	public function image_downsize( $out, $id, $size ) {
		global $_wp_additional_image_sizes;

		// If image size exists let WP serve it like normally
		$imagedata = wp_get_attachment_metadata( $id );
		if ( empty( $size ) || is_array( $size ) || isset( $imagedata['sizes'][ $size ] ) ) {
			return false;
		}

		// Check that the requested size exists, or abort
		if ( ! isset( $_wp_additional_image_sizes[ $size ] ) ) {
			return false;
		}

		// Make the new thumb
		if ( ! $resized = image_make_intermediate_size(
			get_attached_file( $id ),
			$_wp_additional_image_sizes[ $size ]['width'],
			$_wp_additional_image_sizes[ $size ]['height'],
			$_wp_additional_image_sizes[ $size ]['crop']
		)
		) {
			return false;
		}

		// Save image meta, or WP can't see that the thumb exists now
		$imagedata['sizes'][ $size ] = $resized;
		wp_update_attachment_metadata( $id, $imagedata );

		// Return the array for displaying the resized image
		$att_url = wp_get_attachment_url( $id );

		return array( dirname( $att_url ) . '/' . $resized['file'], $resized['width'], $resized['height'], true );
	}

	public function intermediate_image_sizes_advanced( $sizes, $metadata ) {
		return array(
			'thumbnail' => $sizes['thumbnail'],
			'medium'    => $sizes['medium'],
			'large'     => $sizes['large']
		);
	}
}