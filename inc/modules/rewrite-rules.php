<?php

namespace Theme\Module;

class Rewrite_Rules {
	public function __construct() {
		add_action( 'generate_rewrite_rules', array( $this, 'generate_rewrite_rules' ) );
	}

	/**
	 * @param \WP_Rewrite $wp_rewrite
	 *
	 * @return string
	 */
	function generate_rewrite_rules( $wp_rewrite ) {

		$template = wp_normalize_path( get_template_directory() . '/' );
		$path     = str_replace( wp_normalize_path( ABSPATH ), '', $template );
		$path     = str_replace( '\\', '/', $path );

		$wp_rewrite->add_external_rule( $path . 'assets/dist/(css|js)/(.*)-([A-Z-a-z0-9]{32})(.*)',
			$path . 'assets/dist/$1/$2$4' );
	}
}