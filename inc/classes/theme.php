<?php

namespace Theme;

use Theme\Module\Embed;
use Theme\Module\Emoji;
use Theme\Module\Rewrite_Rules;
use Theme\Module\Thumbnail_Demand;
use Theme\Module\Widget_Custom_Classes;

abstract class Base {
	protected $disableEmoji = true;
	protected $disableEmbed = true;

	public function __construct() {
		if ( $this->disableEmoji ) {
			new Emoji( $this );
		}

		if ( $this->disableEmbed ) {
			new Embed( $this );
		}

		new Rewrite_Rules( $this );
		new Thumbnail_Demand( $this );
		new Widget_Custom_Classes( $this );

		add_action( 'after_setup_theme', array( $this, 'setup' ), 5 );
		add_action( 'widgets_init', array( $this, 'register_sidebars' ), 5 );

		add_action( 'admin_bar_menu', array( $this, 'template_details' ), 99 );

		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ), 20 );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ), 200 );

		add_action( 'admin_init', array( $this, 'editor_styles' ) );

		add_action( 'after_switch_theme', array( $this, 'after_switch_theme' ) );

		add_action( 'theme/register_scripts/before', array( $this, 'register_scripts_before' ), 10, 3 );
		add_action( 'theme/register_scripts/after', array( $this, 'register_scripts_after' ), 10, 3 );

		add_filter( 'theme/deps/style', array( $this, 'style_deps' ) );
		add_filter( 'theme/deps/script', array( $this, 'script_deps' ) );

		add_filter( 'theme/deps/style', array( $this, 'both_deps' ) );
		add_filter( 'theme/deps/script', array( $this, 'both_deps' ) );

		add_filter( 'theme/localize/script', array( $this, 'localize_script' ) );

		add_action( 'widgets_init', array( $this, 'register_widgets' ) );

		add_filter( 'script_loader_tag', array( $this, 'script_loader_tag' ), 10, 3 );

		add_filter( 'wpcf7_load_css', '__return_false' );

		//Disable CSS / JS for WPML
		define( 'ICL_DONT_LOAD_NAVIGATION_CSS', true );
		define( 'ICL_DONT_LOAD_LANGUAGES_JS', true );
		define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );
	}

	public function script_loader_tag( $tag, $handle, $src ) {
		if ( $handle === 'google-maps' ) {
			return str_replace( '<script', '<script async defer', $tag );
		}

		return $tag;
	}

	public function after_switch_theme() {
		if ( apply_filters( 'theme/assets/mask', true ) ) {
			add_action( 'admin_init', 'save_mod_rewrite_rules', 1000 );
		}
	}

	public function register_admin_scripts() {
		//Urls
		$urls = $this->get_urls();

		//Register dirs
		$dirs = $this->get_dirs();

		/**
		 * Main styles
		 */
		$style_full = $dirs['css'] . 'admin.css';

		if ( file_exists( $style_full ) ) {
			$ver  = filemtime( $style_full );
			$hash = md5_file( $style_full );
			$url  = $urls['css'] . 'admin-' . $hash . '.css';
			wp_enqueue_style( 'main', $url, array(), $ver );
		}
	}

	/**
	 * Register Widgets
	 */
	public function register_widgets() {
	}

	/**
	 * Deps for CS and JS
	 *
	 * @param array $deps
	 *
	 * @return array
	 */
	public function both_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for JS
	 *
	 * @param array $deps
	 *
	 * @return array
	 */
	public function script_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for css
	 *
	 * @param array $deps
	 *
	 * @return array
	 */
	public function style_deps( $deps ) {
		return $deps;
	}

	/**
	 * Main script localize
	 *
	 * @param array $vars
	 *
	 * @return array
	 */
	public function localize_script( $vars ) {
		return $vars;
	}

	/**
	 * Before main script and style
	 *
	 * @param array $urls
	 * @param array $dirs
	 */
	public function register_scripts_before( $urls, $dirs ) {
	}

	/**
	 * Before main script and style
	 *
	 * @param array $urls
	 * @param array $dirs
	 */
	public function register_scripts_after( $urls, $dirs ) {
	}

	/**
	 * Settings
	 */
	public function setup() {
		load_theme_textdomain( 'theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'custom-background' );

		add_theme_support( 'custom-header' );

		add_theme_support( 'custom-logo' );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		$this->register_menus();
		$this->register_sizes();
	}

	/**
	 * Current template info
	 *
	 * @param WP_Admin_Bar $wp_admin_bar WP_Admin_Bar instance, passed by reference
	 */
	public function template_details( $wp_admin_bar ) {
		global $template;

		$_template   = wp_normalize_path( $template );
		$content_dir = wp_normalize_path( WP_CONTENT_DIR . '/' );
		$_template   = str_replace( $content_dir, '', $_template );

		$wp_admin_bar->add_node(
			array(
				'id'    => 'current-template-file',
				'title' => $_template,
			)
		);
	}

	/**
	 * Register editor style
	 */
	public function editor_styles() {
		$style_filename = 'editor-style.css';
		$style_full     = wp_normalize_path( get_template_directory() . '/assets/dist/css/' . $style_filename );

		if ( file_exists( $style_full ) ) {
			add_editor_style( get_template_directory_uri() . '/assets/dist/css/' . $style_filename . '?t=' . filemtime( $style_full ) );
		}
	}

	/**
	 * Get urls
	 *
	 * @return array
	 */
	public function get_urls() {
		$template_url = trailingslashit( get_template_directory_uri() );

		return array(
			'template' => $template_url,
			'assets'   => $template_url . 'assets/',
			'js'       => $template_url . 'assets/dist/js/',
			'css'      => $template_url . 'assets/dist/css/',
			'vendor'   => $template_url . 'assets/vendor/',
		);
	}

	/**
	 * Get dirs
	 *
	 * @return array
	 */
	public function get_dirs() {
		$dir_path = wp_normalize_path( get_template_directory() . '/' );

		return array(
			'template' => $dir_path,
			'assets'   => wp_normalize_path( $dir_path . 'assets/' ),
			'js'       => wp_normalize_path( $dir_path . 'assets/dist/js/' ),
			'css'      => wp_normalize_path( $dir_path . 'assets/dist/css/' ),
			'vendor'   => wp_normalize_path( $dir_path . 'assets/vendor/' ),
		);
	}

	/**
	 * Register script and styles
	 */
	public function register_scripts() {

		//Urls
		$urls = $this->get_urls();

		//Register dirs
		$dirs = $this->get_dirs();

		do_action( 'theme/register_scripts/before', $urls, $dirs );

		/**
		 * Main styles
		 */
		$style_full = $dirs['css'] . 'style.css';

		if ( file_exists( $style_full ) ) {
			$ver  = filemtime( $style_full );
			$path = apply_filters( 'theme/assets/mask', true ) ? 'style-' . md5_file( $style_full ) . '.css' : 'style.css';

			wp_enqueue_style( 'main', $urls['css'] . $path, apply_filters( 'theme/deps/style', array() ), $ver );
		}

		//Dev File
		if ( defined( 'WP_LIVERELOAD' ) && WP_LIVERELOAD ) {
			wp_enqueue_script( 'livereload', 'http://localhost:35729/livereload.js?snipver=1', null, false, true );
		}

		/**
		 * Main scripts
		 */
		$scripts_full = $dirs['js'] . 'app.js';
		if ( file_exists( $scripts_full ) ) {
			$ver  = filemtime( $scripts_full );
			$path = apply_filters( 'theme/assets/mask', true ) ? 'app-' . md5_file( $scripts_full ) . '.min.js' : 'app.min.js';

			$vars = array(
				'home_url'  => trailingslashit( home_url() ),
				'ajax_url'  => wp_nonce_url( admin_url( 'admin-ajax.php' ), 'theme-ajax' ),
				'template'  => trailingslashit( get_template_directory_uri() ),
				'locale'    => get_locale(),
				'l10n'      => array(),
				'api_url'   => home_url( 'api/v1/' ),
				'api_nonce' => wp_create_nonce( 'wp_rest' ),
			);

			wp_enqueue_script( 'main', $urls['js'] . $path, apply_filters( 'theme/deps/script', array( 'jquery' ) ), $ver, true );
			wp_localize_script( 'main', '__jsVars', apply_filters( 'theme/localize/script', $vars ) );
		}

		do_action( 'theme/register_scripts/after', $urls, $dirs );
	}

	/**
	 * Register custom menu
	 */
	public function register_menus() {
	}

	/**
	 * Register sidebar
	 */
	public function register_sidebars() {
	}

	/**
	 * Register image sizes
	 */
	public function register_sizes() {
	}
}
